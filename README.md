# ViaPushKit

[![CI Status](http://img.shields.io/travis/Elysey/ViaPushKit.svg?style=flat)](https://travis-ci.org/Elysey/ViaPushKit)
[![Version](https://img.shields.io/cocoapods/v/ViaPushKit.svg?style=flat)](http://cocoapods.org/pods/ViaPushKit)
[![License](https://img.shields.io/cocoapods/l/ViaPushKit.svg?style=flat)](http://cocoapods.org/pods/ViaPushKit)
[![Platform](https://img.shields.io/cocoapods/p/ViaPushKit.svg?style=flat)](http://cocoapods.org/pods/ViaPushKit)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

ViaPushKit is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "ViaPushKit"
```

## Author

Elysey, elyseyvova@gmail.com

## License

ViaPushKit is available under the MIT license. See the LICENSE file for more info.
