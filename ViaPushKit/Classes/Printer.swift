//
//  Printer.swift
//  Pods
//
//  Created by Elysey on 16.03.17.
//
//

import Foundation

public struct Printer {
  
  public func log(_ message: String)  {
    print(message)
  }
}
