Pod::Spec.new do |s|
  s.name             = 'ViaPushKit'
  s.version          = '1.0.0.004'
  s.summary          = 'Description of ViaPushKit.'
  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://bitbucket.org/ElyseyVova/viapushkit'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Elysey' => 'elyseyvova@gmail.com' }
  s.source           = { :git => 'https://ElyseyVova@bitbucket.org/ElyseyVova/viapushkit.git', :tag => s.version.to_s }
  s.ios.deployment_target = '8.0'
  s.source_files = 'ViaPushKit/Classes/**/*'
  
  s.resource_bundles = {
     'ViaPushKit' => ['ViaPushKit/Assets/*.framework']
  }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  s.dependency 'VialerSIPLib'
end
